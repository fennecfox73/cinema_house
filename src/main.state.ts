import { InitialState } from './main.state.types';

export const initialState: InitialState = {
    homePageMovies: [],
    selectedMovie: 0,
    isMoviePageOpened: false,
    searchedMovie: '',
    pageSrolledTill: 1,
    isDarkMode: true,
    selectedCategory: 'Favourites',
};
