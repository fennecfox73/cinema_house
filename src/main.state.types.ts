import { Categories } from './components/Profile/Profile.types';
import { Movie } from './services/movies.services';
export interface InitialState {
    homePageMovies: Movie[];
    selectedMovie: number;
    isMoviePageOpened: boolean;
    searchedMovie: string;
    pageSrolledTill: number;
    isDarkMode: boolean;
    selectedCategory: Categories;
}
