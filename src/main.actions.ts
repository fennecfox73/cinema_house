import { Movie } from './services/movies.services';

export const HOME_PAGE_MOVIES_ADDED = 'ADD_HOME_PAGE_MOVIES';
export const MOVIE_SELECTED = 'CHANGE_SELECTED_MOVIE';
export const MOVIE_PAGE_OPENED = 'IS_MOVIE_PAGE_OPENED';
export const SEARCHED_MOVIE_SET = 'SEARCHED_MOVIE';
export const CURRENT_PAGE_SET = 'SET_CURRENT_PAGE';
export const IS_DARK_MODE_ON = 'IS_DARK_MODE_ON';

export function showMoviesAtHomePage(additionalMovies: Movie[]): {
    type: string;
    homePageMovies: Movie[];
} {
    return {
        type: HOME_PAGE_MOVIES_ADDED,
        homePageMovies: additionalMovies,
    };
}

export function selectMovie(id: number): {
    type: string;
    selectedMovie: number;
} {
    return {
        type: MOVIE_SELECTED,
        selectedMovie: id,
    };
}

export function changeSearchedMovie(phrase: string): {
    type: string;
    searchedMovie: string;
} {
    return {
        type: SEARCHED_MOVIE_SET,
        searchedMovie: phrase,
    };
}

export function changePageSrolledTill(pageSrolledTill: number): {
    type: string;
    pageSrolledTill: number;
} {
    return {
        type: CURRENT_PAGE_SET,
        pageSrolledTill: pageSrolledTill,
    };
}

export function isMoviePageOpened(isMovieOpened: boolean): {
    type: string;
    isMoviePageOpened: boolean;
} {
    return {
        type: MOVIE_PAGE_OPENED,
        isMoviePageOpened: isMovieOpened,
    };
}

export function IsDarkModeOn(IsDark: boolean): {
    type: string;
    isDarkMode: boolean;
} {
    return {
        type: IS_DARK_MODE_ON,
        isDarkMode: IsDark,
    };
}
