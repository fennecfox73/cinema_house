import React, { useEffect } from 'react';
import HomePage from './components/HomePage/HomePage';
import { fetchAllMovies } from './services/movies.services';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { showMoviesAtHomePage } from './main.actions';
import Profile from './components/Profile/Profile';
import useStyles from './App.styles';
import { createTheme, ThemeProvider } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { getTheme } from './theme';
import { RootState } from './store';
import RegisterForm from './components/Login/Register/RegisterForm';
import PrivacyPolicyPage from './components/GDPR/PrivacyPolicyPage/PrivacyPolicyPage';
import LoginForm from './components/Login/Login/LoginForm';
import PageNotFound from './components/PageNotFound/PageNotFound';
import MoviePage from './components/MoviePage/MoviePage';
import { getUserName } from './utils/common.utils';
import { addFavMovie } from './components/Profile/Profile.actions';

function App() {
    const dispatch = useDispatch();
    const classes = useStyles();
    const isDarkMode = useSelector((state: RootState) => state.movies.isDarkMode);
    const mode = isDarkMode ? 'dark' : 'light';
    const currentUser = getUserName();

    window.onbeforeunload = function () {
        window.scrollTo(0, 0);
    };

    // detect browser back button click in order to fetch new movie info
    window.onpopstate = function () {
        window.location.reload();
    };

    useEffect(() => {
        fetchAllMovies('1')
            .then(movies => dispatch(showMoviesAtHomePage(movies)))
            .catch(() => dispatch(showMoviesAtHomePage([])));

        const userData = {
            userName: currentUser,
        };

        fetch('http://localhost:8082/favouritedmovies/', {
            method: 'POST',
            body: JSON.stringify(userData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }).then(async response => {
            const favedMoviesResponse = await response.json();

            if (response.status === 200) {
                dispatch(addFavMovie(favedMoviesResponse));
                return true;
            } else {
                return favedMoviesResponse.errorMsg;
            }
        });
    }, []);

    // Update the theme only if the mode changes
    const theme = React.useMemo(() => createTheme(getTheme(mode)), [mode]);

    return (
        <ThemeProvider theme={theme}>
            <CssBaseline />
            <div className={classes.root}>
                <div className={classes.container}>
                    <BrowserRouter>
                        <Routes>
                            <Route path="/register" element={<RegisterForm />} />
                            <Route path="/login" element={<LoginForm />} />
                            <Route path="/profile" element={<Profile />} />
                            <Route path="/movie/:movieid" element={<MoviePage />} />
                            <Route path="/privacypolicy" element={<PrivacyPolicyPage />} />
                            <Route path="/" element={<HomePage />} />
                            <Route path="*" element={<PageNotFound />} />
                        </Routes>
                    </BrowserRouter>
                </div>
            </div>
        </ThemeProvider>
    );
}
export default App;
