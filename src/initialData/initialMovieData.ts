import noImage from '../images/no-image-available.png';

export const initialMovieData = {
    id: 0,
    title: '',
    vote_average: 0,
    overview: '',
    poster_path: noImage,
    release_date: '',
    runtime: 0,
    budget: 0,
    revenue: 0,
    genres: [],
    production_countries: [],
};
