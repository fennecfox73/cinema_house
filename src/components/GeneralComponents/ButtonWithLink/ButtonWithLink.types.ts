export interface ButtonWithLinkProps {
    linkForNavigation: string;
    handleButtonClick?: React.MouseEventHandler<HTMLButtonElement> | undefined;
    text: string;
    icon: JSX.Element;
}
