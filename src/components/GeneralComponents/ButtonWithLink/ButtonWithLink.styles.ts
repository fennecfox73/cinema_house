import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    profileButton: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.text.secondary,
        textTransform: 'capitalize',
        fontSize: '1rem',
        [theme.breakpoints.up('xs')]: {
            padding: '12px 15px',
        },
        [theme.breakpoints.up('sm')]: {
            padding: '12px 30px',
            margin: 'auto 40px',
        },
    },
}));
