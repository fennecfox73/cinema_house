import React from 'react';
import { Button } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import useStyles from './ButtonWithLink.styles';
import { ButtonWithLinkProps } from './ButtonWithLink.types';

const ButtonWithLink = ({
    linkForNavigation,
    handleButtonClick: handleProfileButtonClick,
    text,
    icon,
}: ButtonWithLinkProps) => {
    const classes = useStyles();

    return (
        <NavLink to={linkForNavigation}>
            <Button variant="outlined" onClick={handleProfileButtonClick} className={classes.profileButton}>
                {text}
                {icon}
            </Button>
        </NavLink>
    );
};

export default ButtonWithLink;
