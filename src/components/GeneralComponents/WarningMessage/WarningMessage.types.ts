import { Color } from '@material-ui/lab/Alert';

export interface WarningMessageProps {
    message: string;
    severity?: Color;
}
