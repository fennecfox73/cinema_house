import React from 'react';
import { Alert } from '@material-ui/lab';
import { WarningMessageProps } from './WarningMessage.types';

const WarningMessage: React.FC<WarningMessageProps> = ({ message, severity = 'error' }: WarningMessageProps) => {
    return (
        <Alert variant="filled" severity={severity}>
            {message}
        </Alert>
    );
};

export default WarningMessage;
