import React from 'react';
import { Alert } from '@material-ui/lab';
import { WarningMessagePopupProps } from './WarningMessagePopup.types';
import useStyles from './WarningMessagePopup.styles';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { setWarningPopupMessage } from '../../Profile/Profile.actions';
import cn from 'clsx';

const WarningMessagePopup: React.FC<WarningMessagePopupProps> = ({ severity = 'error' }: WarningMessagePopupProps) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const message = useSelector((state: RootState) => state.profile.warningPopupMessageSet);

    const shouldHideWarningMessage = message === '';

    setTimeout(() => {
        const warning = document.getElementById('warningPopup');

        if (warning) {
            warning.style.display = 'none';
        }

        dispatch(setWarningPopupMessage(''));
    }, 3000);

    return (
        <Alert
            id="warningPopup"
            variant="filled"
            severity={severity}
            className={cn([classes.container, shouldHideWarningMessage && classes.shouldBeHidden])}
        >
            {message}
        </Alert>
    );
};

export default WarningMessagePopup;
