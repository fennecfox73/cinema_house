import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.primary.main,
        position: 'fixed',
        width: '100%',
        bottom: '0',
        left: '0',
        right: '0',
        padding: '20px',
        display: 'flex',
        justifyContent: 'center',
        '& a': {
            color: 'inherit',
        },
    },
    shouldBeHidden: {
        display: 'none',
    },
}));
