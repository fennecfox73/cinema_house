import { Color } from '@material-ui/lab/Alert';

export interface WarningMessagePopupProps {
    severity?: Color;
}
