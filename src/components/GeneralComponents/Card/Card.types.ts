import { Movie } from '../../../services/movies.services';

export interface CardProps {
    card: Movie;
}
