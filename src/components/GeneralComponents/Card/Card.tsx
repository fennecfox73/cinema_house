import React from 'react';
import { selectMovie, isMoviePageOpened } from '../../../main.actions';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import { Grid, IconButton } from '@material-ui/core';
import noImage from '../../../images/no-image-available.png';
import { NavLink, useLocation } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import useStyles from './Card.styles';
import { RootState } from '../../../store';
import { CardProps } from './Card.types';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { getUserName, isUserLoggedIn as isCurrentUserLoggedIn } from '../../../utils/common.utils';
import { favouriteMovie, removeFavouriteMovie } from '../../../utils/movies.utils';
const posterBaseUrl = 'https://image.tmdb.org/t/p/w300';

const Card: React.FC<CardProps> = ({ card }: CardProps) => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const { pathname } = useLocation();

    const favouriteMovies = useSelector((state: RootState) => state.profile.favouriteMovies);

    const isProfilePageOpened = String(pathname.split('/').pop()) === 'profile' ? true : false;
    const isUserLoggedIn = isCurrentUserLoggedIn();
    const isMovieFaved = favouriteMovies.includes(card.id);
    const currentUser = getUserName();

    const changeIsMovieFavourite = () => {
        isMovieFaved
            ? dispatch(removeFavouriteMovie({ userName: currentUser, movie: card.id }))
            : dispatch(favouriteMovie({ userName: currentUser, movie: card.id }));
    };

    const SetSelectedMovieId = (movieId: number) => {
        dispatch(isMoviePageOpened(true));
        dispatch(selectMovie(movieId));
    };

    return (
        <Grid item key={card.id}>
            <div className="card-container" onClick={() => SetSelectedMovieId(card.id)}>
                <NavLink to={'/movie/' + card.id} data-testid="catalog-card">
                    <img
                        className={isProfilePageOpened ? classes.profileCard : classes.primaryCard}
                        alt={'Poster of ' + card.title}
                        src={
                            card.poster_path
                                ? card.poster_path.includes('.jpg')
                                    ? posterBaseUrl + card.poster_path
                                    : noImage
                                : noImage
                        }
                        title={card.title}
                    />
                </NavLink>
                <div className="card-details">
                    <IconButton className="fav-icon" onClick={changeIsMovieFavourite}>
                        <NavLink to={isUserLoggedIn ? '' : '/login'} className="fav-icon-button">
                            {isMovieFaved ? <FavoriteIcon /> : <FavoriteBorderIcon />}
                        </NavLink>
                    </IconButton>
                    <div className="vote-average">{card.vote_average}</div>
                </div>
            </div>
        </Grid>
    );
};

export default Card;
