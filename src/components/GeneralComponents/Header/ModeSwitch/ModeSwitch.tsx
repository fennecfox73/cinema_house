import React, { memo } from 'react';
import { Switch } from '@material-ui/core';
import { useDispatch, useSelector } from 'react-redux';
import NightsStayIcon from '@material-ui/icons/NightsStay';
import useStyles from './ModeSwitch.styles';
import { RootState } from '../../../../store';
import { IsDarkModeOn } from '../../../../main.actions';

const ModeSwitch: React.FC = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const isDarkMode = useSelector((state: RootState) => state.movies.isDarkMode);

    const changeCssMode = () => {
        dispatch(IsDarkModeOn(!isDarkMode));
    };

    return (
        <div className={classes.modeSwitch}>
            <Switch onClick={changeCssMode} checked={isDarkMode} color="secondary" data-testid="header-mode-switch" />
            <NightsStayIcon className={`${classes.modeSwitchIcon} ${classes.modeSwitchIconOn}`} />
        </div>
    );
};

export default memo(ModeSwitch);
