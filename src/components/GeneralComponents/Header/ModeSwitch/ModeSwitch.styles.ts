import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    modeSwitch: {
        [theme.breakpoints.up('xs')]: {
            minWidth: '90px',
        },
        [theme.breakpoints.up('md')]: {
            width: 'auto',
        },
    },
    modeSwitchIcon: {
        fontSize: '1.7rem',
        color: theme.palette.primary.contrastText,
    },
    modeSwitchIconOn: {
        marginBottom: '-7px',
    },
}));
