import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    searchField: {
        marginLeft: 'auto',
        marginRight: '1.5rem',
        '&:hover': {
            backgroundColor: theme.palette.primary.dark,
        },
    },
}));
