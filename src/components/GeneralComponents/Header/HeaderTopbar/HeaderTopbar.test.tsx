import React from 'react';

import Topbar from './HeaderTopbar';

import Enzyme, { shallow } from 'enzyme';
import Adapter from '@wojtekmaj/enzyme-adapter-react-17';
import { Provider } from 'react-redux';
import store from '../../../../store';
import { BrowserRouter } from 'react-router-dom';

Enzyme.configure({ adapter: new Adapter() });

describe('Test HeaderTopbar', () => {
    it('matches snapshot', () => {
        const wrapper = shallow(
            <Provider store={store}>
                <BrowserRouter>
                    <Topbar />
                </BrowserRouter>
            </Provider>,
        );

        expect(wrapper).toMatchSnapshot();
    });
});
