import React from 'react';
import { Toolbar } from '@material-ui/core';
import { NavLink } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { isMoviePageOpened, changeSearchedMovie } from '../../../../main.actions';
import PersonOutlineOutlinedIcon from '@material-ui/icons/PersonOutlineOutlined';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import useStyles from './HeaderTopbar.styles';
import SearchBox from '../SearchBox/SearchBox';
import { useLocation } from 'react-router-dom';
import { selectCategory } from '../../../Profile/Profile.actions';
import ModeSwitch from '../ModeSwitch/ModeSwitch';
import ButtonWithLink from '../../ButtonWithLink/ButtonWithLink';
import { isUserLoggedIn as isCurrentUserLoggedIn, getUserName } from '../../../../utils/common.utils';
import { textConstants } from '../../../../constants';

const Topbar: React.FC = () => {
    const dispatch = useDispatch();
    const classes = useStyles();
    const { pathname } = useLocation();

    const isUserLoggedIn = isCurrentUserLoggedIn();
    const username = getUserName();
    const isProfilePageOpened = String(pathname.split('/').pop()) === 'profile' ? true : false;

    let linkForNavigation;
    let buttonText;
    let buttonIcon;

    if (!isProfilePageOpened) {
        linkForNavigation = isUserLoggedIn ? '/profile' : '/login';
        buttonText = `${username} Profile`;
        buttonIcon = <PersonOutlineOutlinedIcon className={classes.profileButtonIcon} />;
    } else {
        linkForNavigation = '/';
        buttonText = 'Logout';
        buttonIcon = <ExitToAppIcon className={classes.profileButtonIcon} />;
    }

    const handleHomePageLink = () => {
        dispatch(changeSearchedMovie(''));
        dispatch(isMoviePageOpened(false));
        dispatch(selectCategory('Favourites'));
        window.scrollTo(0, 0);
    };

    const handleProfileButtonClick = () => {
        if (isProfilePageOpened && isUserLoggedIn) {
            const user = {
                username: '',
                isLoggedIn: false,
            };

            localStorage.setItem('user', JSON.stringify(user));
        }
    };

    return (
        <Toolbar className={classes.container} data-testid="header">
            <NavLink to={'/'} onClick={handleHomePageLink}>
                <h1 className={classes.title}>{textConstants.CINEMA_HOUSE}</h1>
            </NavLink>
            <SearchBox />
            <ButtonWithLink
                linkForNavigation={linkForNavigation}
                handleButtonClick={handleProfileButtonClick}
                text={buttonText}
                icon={buttonIcon}
            />
            <ModeSwitch />
        </Toolbar>
    );
};

export default Topbar;
