import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.background.default,
        [theme.breakpoints.up('sm')]: {
            alignItems: 'center',
            alignContent: 'flex-start',
            justifyContent: 'space-around',
            flexDirection: 'row',
            flexWrap: 'wrap',
            paddingBottom: '1px',
        },
        [theme.breakpoints.up('sm')]: {},
        ['@media (min-width:300px)']: {
            paddingLeft: '16px',
            paddingRight: '16px',
        },
        [theme.breakpoints.up('xs')]: {
            position: 'fixed',
            top: 0,
            left: 0,
            right: 0,
            alignItems: 'center',
            '& a': {
                textDecoration: 'none',
                color: theme.palette.text.primary,
            },
        },
    },
    title: {
        fontWeight: 500,
        color: theme.palette.text.primary,
        [theme.breakpoints.up('xs')]: {
            fontSize: '1.5rem',
            textAlign: 'center',
        },
        [theme.breakpoints.up('sm')]: {
            fontSize: '1.8rem',
        },
    },
    profileButtonIcon: {
        fontSize: '1.2rem',
        marginLeft: '5px',
    },
}));
