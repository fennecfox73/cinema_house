import Grid from '@material-ui/core/Grid';
import React from 'react';
import useStyles from './Footer.styles';
import { SocialIcon } from 'react-social-icons';
// import MovieFilter from '@material-ui/icons/MovieFilter';
import catGif from '../../../images/puss-in-boots-cat.gif';

const Footer: React.FC = () => {
    const classes = useStyles();

    return (
        <Grid className={classes.container}>
            <Grid item xs={12}>
                © 2023 | Designed & coded by Ruta Monkiene 👩‍💻
            </Grid>
            <Grid item xs={12} className={classes.socialIcons}>
                <SocialIcon url="https://gitlab.com/rumone7" target="_blank" />
                <SocialIcon url="https://www.linkedin.com/in/rutamonkiene/" target="_blank" />
            </Grid>
            <Grid xs={12} className={classes.easterEgg}>
                <img src={catGif} alt="loading..." />
            </Grid>
        </Grid>
    );
};

export default Footer;
