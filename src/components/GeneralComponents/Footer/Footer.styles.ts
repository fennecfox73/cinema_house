import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        textAlign: 'center',
        margin: '5rem',
        fontSize: '1rem',
        letterSpacing: '0.01em',
        ['@media (max-width:400px)']: {
            fontSize: '0.8rem',
        },
    },
    socialIcons: {
        marginTop: '3rem',
        '& a': {
            margin: '0.5rem',
        },
    },
    easterEgg: {
        marginTop: '3rem',
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    easterEggIcon: {
        fontSize: '10rem',
    },
}));
