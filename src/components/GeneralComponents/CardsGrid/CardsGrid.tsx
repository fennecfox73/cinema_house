import React from 'react';
import { Grid } from '@material-ui/core';
import Card from '../Card/Card';
import useStyles from './CardsGrid.styles';
import { CardsGridProps } from './CardGrid.types';

const CardsGrid: React.FC<CardsGridProps> = ({ movies }: CardsGridProps) => {
    const classes = useStyles();

    return (
        <Grid container className={classes.cardsContainer}>
            {movies!.length > 0 &&
                movies!.filter(movie => movie.vote_average !== 0).map(movie => <Card key={movie.id} card={movie} />)}
        </Grid>
    );
};

export default CardsGrid;
