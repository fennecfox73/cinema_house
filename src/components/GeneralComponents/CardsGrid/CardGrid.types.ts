import { Movie } from '../../../services/movies.services';

export interface CardsGridProps {
    movies: Movie[] | undefined;
}
