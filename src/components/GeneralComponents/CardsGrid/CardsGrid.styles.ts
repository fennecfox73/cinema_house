import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
    cardsContainer: {
        width: '100%',
        display: 'flex',
        justifyContent: 'center',
    },
}));
