import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    button: {
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.text.secondary,
        textTransform: 'capitalize',
        fontSize: '1rem',
        marginTop: '3px',
        '&:hover': {
            backgroundColor: '#f59cae',
        },
    },
    primaryButton: {
        backgroundColor: theme.palette.primary.main,
    },
    secondaryButton: {
        backgroundColor: theme.palette.primary.light,
    },
    buttonIcon: {
        marginLeft: '10px',
        lineHeight: '0px',
        '& svg': {
            fontSize: '1.2rem',
        },
    },
}));
