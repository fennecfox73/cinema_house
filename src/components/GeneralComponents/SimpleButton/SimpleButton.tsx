import Button from '@material-ui/core/Button';
import React from 'react';
import useStyles from './SimpleButton.styles';
import cn from 'clsx';
export interface SimpleButtonProps {
    text: string;
    icon: JSX.Element;
    handleOnClick: React.MouseEventHandler<HTMLButtonElement> | undefined;
    isPrimary: boolean;
}

const SimpleButton: React.FC<SimpleButtonProps> = ({
    text,
    icon,
    handleOnClick,
    isPrimary = true,
}: SimpleButtonProps) => {
    const classes = useStyles();

    return (
        <Button
            onClick={handleOnClick}
            variant="contained"
            className={cn([classes.button, isPrimary ? classes.primaryButton : classes.secondaryButton])}
        >
            {text}
            <span className={classes.buttonIcon}>{icon}</span>
        </Button>
    );
};

export default SimpleButton;
