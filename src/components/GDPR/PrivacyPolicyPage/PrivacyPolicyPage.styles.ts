import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        color: theme.palette.text.primary,
        padding: '30px 50px',
        [theme.breakpoints.down('sm')]: {
            padding: '20px 40px',
        },
        [theme.breakpoints.down('xs')]: {
            padding: '10px',
        },
        '& a': {
            color: 'inherit',
        },
    },
}));
