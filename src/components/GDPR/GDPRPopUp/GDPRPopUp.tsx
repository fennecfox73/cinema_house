import Typography from '@material-ui/core/Typography';
import React from 'react';
import { NavLink } from 'react-router-dom';
import SimpleButton from '../../GeneralComponents/SimpleButton/SimpleButton';
import useStyles from './GDPRPopUp.styles';
import ThumbUpIcon from '@material-ui/icons/ThumbUp';
import NotInterestedIcon from '@material-ui/icons/NotInterested';
import Cookies from 'universal-cookie';
import Button from '@material-ui/core/Button';
import cn from 'clsx';
import { useDispatch, useSelector } from 'react-redux';
import { setGDPRPopUpButtonClicked } from '../../Profile/Profile.actions';
import { RootState } from '../../../store';
import { textConstants } from '../../../constants';

const GDPRPopUp: React.FC = () => {
    const classes = useStyles();
    const cookies = new Cookies();
    const dispatch = useDispatch();
    const isGDPRPopUpButtonClicked = useSelector((state: RootState) => state.profile.isGDPRPopUpButtonClicked);
    const cookieConsent = cookies.get('CookieConsent') ? cookies.get('CookieConsent') : false;
    const shouldGDPRPopUpBeHidden = isGDPRPopUpButtonClicked || cookieConsent ? true : false;

    const setCookieConcentValue = (isCookieConsentGiven: boolean) => {
        cookies.set('CookieConsent', { isConsentGiven: isCookieConsentGiven }, { path: '/' });
    };

    const handleRejectButtonClick = () => {
        setCookieConcentValue(false);
        dispatch(setGDPRPopUpButtonClicked(true));
    };

    const handleAcceptButtonClick = () => {
        setCookieConcentValue(true);
        dispatch(setGDPRPopUpButtonClicked(true));
    };

    return (
        <div className={cn([classes.container, shouldGDPRPopUpBeHidden && classes.containerHidden])}>
            <Typography>
                <span className={classes.messageText}>
                    This website stores cookies or similar technologies on your computer. These cookies are used to
                    allow us to remember you. We use this information in order to customize your browsing experience. To
                    find out more about the cookies we use, see our&nbsp;
                    <NavLink to={'/privacypolicy'}>Privacy Policy.</NavLink>
                    &nbsp;&nbsp;
                </span>
                <SimpleButton
                    text={'Not Now'}
                    icon={<NotInterestedIcon />}
                    handleOnClick={handleRejectButtonClick}
                    isPrimary={false}
                />
                <Button onClick={handleAcceptButtonClick} variant="contained" className={classes.button}>
                    {textConstants.ACCEPT}
                    <span className={classes.buttonIcon}>
                        <ThumbUpIcon />
                    </span>
                </Button>
            </Typography>
        </div>
    );
};

export default GDPRPopUp;
