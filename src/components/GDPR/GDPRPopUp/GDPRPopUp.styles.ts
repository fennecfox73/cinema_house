import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.primary.main,
        position: 'fixed',
        width: '100%',
        bottom: '0',
        left: '0',
        right: '0',
        padding: '20px',
        '& a': {
            color: 'inherit',
        },
    },
    containerHidden: {
        display: 'none',
    },
    messageText: {
        [theme.breakpoints.down('sm')]: {
            display: 'block',
            paddingBottom: '10px',
        },
        [theme.breakpoints.down('xs')]: {
            paddingBottom: '5px',
        },
    },
    button: {
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.text.secondary,
        textTransform: 'capitalize',
        fontSize: '1rem',
        marginTop: '3px',
        marginLeft: '10px',
        minWidth: '123px',
        '&:hover': {
            backgroundColor: '#f59cae',
        },
    },
    buttonIcon: {
        marginLeft: '10px',
        lineHeight: '0px',
        '& svg': {
            fontSize: '1.2rem',
        },
    },
}));
