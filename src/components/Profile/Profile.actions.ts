import { Categories } from './Profile.types';

export const CATEGORY_SELECTED = 'CATEGORY_SELECTED';
export const USERNAME_SET = 'USERNAME_SET';
export const SHOULD_SHOW_WARNING_MESSAGE_SET = 'SHOULD_SHOW_WARNING_MESSAGE_SET';
export const WARNING_MESSAGE_SET = 'WARNING_MESSAGE_SET';
export const GDPR_POPUP_BUTTON_CLICKED = 'GDPR_POPUP_BUTTON_CLICKED';
export const FAVOURITE_MOVIE_ADDED = 'FAVOURITE_MOVIE_ADDED';
export const FAVOURITE_MOVIE_REMOVED = 'FAVOURITE_MOVIE_REMOVED';
export const WARNING_POPUP_MESSAGE = 'WARNING_POPUP_MESSAGE';

export function selectCategory(category: Categories): {
    type: string;
    selectedCategory: Categories;
} {
    return {
        type: CATEGORY_SELECTED,
        selectedCategory: category,
    };
}

export function setUsername(username: string): {
    type: string;
    username: string;
} {
    return {
        type: USERNAME_SET,
        username: username,
    };
}

export function setShouldShowWarningMessage(shouldShowWarningMessage: boolean): {
    type: string;
    setShouldShowWarningMessage: boolean;
} {
    return {
        type: SHOULD_SHOW_WARNING_MESSAGE_SET,
        setShouldShowWarningMessage: shouldShowWarningMessage,
    };
}

export function setWarningMessage(warningMessage: string): {
    type: string;
    warningMessageSet: string;
} {
    return {
        type: WARNING_MESSAGE_SET,
        warningMessageSet: warningMessage,
    };
}

export function setGDPRPopUpButtonClicked(isClicked: boolean): {
    type: string;
    isGDPRPopUpButtonClicked: boolean;
} {
    return {
        type: GDPR_POPUP_BUTTON_CLICKED,
        isGDPRPopUpButtonClicked: isClicked,
    };
}

export function addFavMovie(movie: number): {
    type: string;
    favMovie: number;
} {
    return {
        type: FAVOURITE_MOVIE_ADDED,
        favMovie: movie,
    };
}

export function removeFavMovie(movie: number): {
    type: string;
    favMovie: number;
} {
    return {
        type: FAVOURITE_MOVIE_REMOVED,
        favMovie: movie,
    };
}

export function setWarningPopupMessage(warningPopupMessageSet: string): {
    type: string;
    warningPopupMessageSet: string;
} {
    return {
        type: WARNING_POPUP_MESSAGE,
        warningPopupMessageSet: warningPopupMessageSet,
    };
}
