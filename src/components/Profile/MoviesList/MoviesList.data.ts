import { Movie } from '../../../services/movies.services';

export interface MoviesListProps {
    movies: Movie[];
    errorMessage: string;
}
