import React from 'react';
import CardsGrid from '../../GeneralComponents/CardsGrid/CardsGrid';
import ErrorMessage from './ErrorMessage/ErrorMessage';
import { MoviesListProps } from './MoviesList.data';

const MoviesList: React.FC<MoviesListProps> = ({ movies, errorMessage }) => {
    return <>{movies.length > 0 ? <CardsGrid movies={movies} /> : <ErrorMessage errorMessage={errorMessage} />}</>;
};

export default MoviesList;
