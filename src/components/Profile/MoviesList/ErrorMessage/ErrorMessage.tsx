import React from 'react';
import useStyles from './ErrorMessage.styles';
import { ErrorMessageProps } from './ErrorMessage.types';

const ErrorMessage: React.FC<ErrorMessageProps> = ({ errorMessage }) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <p className={classes.message}>{errorMessage}</p>
        </div>
    );
};

export default ErrorMessage;
