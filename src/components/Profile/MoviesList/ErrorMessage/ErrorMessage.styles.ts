import { makeStyles } from '@material-ui/core';

export default makeStyles(() => ({
    container: {
        display: 'flex',
        height: '60%',
    },
    message: {
        alignSelf: 'center',
    },
}));
