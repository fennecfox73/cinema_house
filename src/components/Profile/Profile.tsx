import { AppBar, Grid } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Movie, fetchSelectedMovie } from '../../services/movies.services';
import Footer from '../GeneralComponents/Footer/Footer';
import Drawer from './Drawer/Drawer';
import useStyles from './Profile.styles';
import { RootState } from '../../store';
import Settings from './Settings/Settings';
import GDPRPopUp from '../GDPR/GDPRPopUp/GDPRPopUp';
import HeaderTopbar from '../GeneralComponents/Header/HeaderTopbar/HeaderTopbar';
import { getUserName, isUserLoggedIn as isCurrentUserLoggedIn } from '../../utils/common.utils';
import MoviesList from './MoviesList/MoviesList';
import { Navigate } from 'react-router-dom';

const Profile: React.FunctionComponent = () => {
    const classes = useStyles();
    const selectedCategory = useSelector((state: RootState) => state.profile.selectedCategory);
    const currentUser = getUserName();

    const isUserLoggedIn = isCurrentUserLoggedIn();

    const noMoviesYetMessage = 'You have no favourite movies yet 👻';

    const [favouriteMoviesIds, setFavouriteMoviesIds] = useState<number[]>([]);
    const [favouriteMovies, setFavouriteMovies] = useState<Movie[]>([]);
    const [errorMessage, setErrorMessage] = useState<string>('');

    const getFavouriteMovies = async () => {
        const userData = {
            userName: currentUser,
        };

        fetch('http://localhost:8082/favouritedmovies/', {
            method: 'POST',
            body: JSON.stringify(userData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }).then(async response => {
            const favedMovies = await response.json();

            if (response.status === 200) {
                setFavouriteMoviesIds(favedMovies);
                setErrorMessage('');
            } else {
                setErrorMessage('Something went wrong getting the movies.');
            }
        });
    };

    useEffect(() => {
        (async function () {
            getFavouriteMovies();

            window.scrollTo(0, 0);
        })();
    }, []);

    useEffect(() => {
        const moviesAsPromises = favouriteMoviesIds.map(async movieId => await fetchSelectedMovie(movieId));

        (async function () {
            const favedMovies = await Promise.all(moviesAsPromises);

            setFavouriteMovies(favedMovies);
        })();
    }, [favouriteMoviesIds]);

    return isUserLoggedIn ? (
        <div className={classes.container}>
            <AppBar>
                <HeaderTopbar />
            </AppBar>
            <div className={classes.profileContent}>
                <Drawer></Drawer>
                <div className={classes.categoryContentContainer}>
                    <Grid container>
                        <Grid item xs={12}>
                            <div className={classes.title}>{selectedCategory}</div>
                        </Grid>
                        <Grid item xs={12}>
                            {selectedCategory === 'Settings' ? (
                                <Settings />
                            ) : (
                                <MoviesList
                                    movies={favouriteMovies}
                                    errorMessage={errorMessage ? errorMessage : noMoviesYetMessage}
                                />
                            )}
                        </Grid>
                    </Grid>
                </div>
            </div>
            <Footer />
            <GDPRPopUp />
        </div>
    ) : (
        <Navigate to="/" />
    );
};

export default Profile;
