import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    icon: {
        color: theme.palette.text.primary,
    },
}));
