import { ListItem, ListItemIcon, ListItemText } from '@material-ui/core';
import * as React from 'react';
import useStyles from './DrawerCategory.styles';
import { useDispatch } from 'react-redux';
import { selectCategory } from '../Profile.actions';
import { DrawerCategoryProps } from './DrawerCategory.types';

const DrawerCategory: React.FC<DrawerCategoryProps> = ({ category, className, icon }: DrawerCategoryProps) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const handleCategoryClick = () => {
        switch (category) {
            case 'Favourites':
                dispatch(selectCategory('Favourites'));
                break;
            case 'Explore':
                dispatch(selectCategory('Explore'));
                break;
            case 'Watchlist':
                dispatch(selectCategory('Watchlist'));
                break;
            case 'Settings':
                dispatch(selectCategory('Settings'));
                break;
            case 'Log out':
                break;
            default:
                console.error(`Sorry, your choosen category doesn't exist 👻`);
        }
    };

    return (
        <ListItem button onClick={handleCategoryClick} className={className}>
            <ListItemIcon className={classes.icon}>{icon}</ListItemIcon>
            <ListItemText primary={category} />
        </ListItem>
    );
};

export default DrawerCategory;
