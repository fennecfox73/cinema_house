import { Categories } from '../Profile.types';

export interface DrawerCategoryProps {
    category: Categories;
    className?: string | undefined;
    icon: JSX.Element;
}
