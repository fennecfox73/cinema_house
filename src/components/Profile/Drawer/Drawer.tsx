import { Divider, List } from '@material-ui/core';
import * as React from 'react';
import useStyles from './Drawer.styles';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import SettingsSuggestIcon from '@material-ui/icons/Settings';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import DrawerCategory from './DrawerCategory';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { NavLink } from 'react-router-dom';
import cn from 'clsx';
import { setShouldShowWarningMessage, setWarningMessage } from '../Profile.actions';

const SideDrawer: React.FC = () => {
    const classes = useStyles();
    const selectedCategory = useSelector((state: RootState) => state.profile.selectedCategory);
    const user = JSON.parse(localStorage.getItem('user') || '{}');
    const username = user.username;
    const dispatch = useDispatch();

    const LogOut = () => {
        dispatch(setWarningMessage(''));
        dispatch(setShouldShowWarningMessage(false));

        const user = {
            username: '',
            isLoggedIn: false,
        };

        localStorage.setItem('user', JSON.stringify(user));
    };

    return (
        <div className={classes.drawerContainer}>
            <div className={classes.profileDetails}>
                <div className={classes.profilePictureContainer}>
                    <AccountCircleIcon className={classes.profilePicture} />
                </div>
                <div className={classes.profileNameSection}>
                    <p className={classes.profileName}>{username}</p>
                </div>
            </div>
            <Divider className={classes.divider} />
            <List>
                <DrawerCategory
                    category={'Favourites'}
                    className={cn([classes.category, selectedCategory === 'Favourites' ?? classes.selectedCategory])}
                    icon={<FavoriteBorderIcon />}
                />
                <DrawerCategory
                    category={'Settings'}
                    className={cn([classes.category, selectedCategory === 'Settings' ?? classes.selectedCategory])}
                    icon={<SettingsSuggestIcon />}
                />
            </List>
            <Divider />
            <List className={cn([classes.secondaryCategory, classes.category])}>
                <NavLink to={'/'} className={cn([classes.link, classes.category])} onClick={LogOut}>
                    <DrawerCategory category={'Log out'} className={classes.category} icon={<ExitToAppIcon />} />
                </NavLink>
            </List>
        </div>
    );
};

export default SideDrawer;
