import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    drawerContainer: {
        width: 240,
        margin: '20px',
        position: 'relative',
    },
    profileDetails: {
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    profilePictureContainer: {
        marginTop: '15px',
    },
    profilePicture: {
        height: '3rem',
        width: '3rem',
    },
    profileNameSection: {
        marginLeft: '1rem',
        display: 'flex',
        flexDirection: 'column',
    },
    profileName: {
        fontSize: '16px',
        lineHeight: '1.7rem',
    },
    profileSecondaryInfo: {
        fontSize: '12px',
    },
    divider: {
        backgroundColor: theme.palette.text.primary,
        width: '80%',
        margin: '20px auto',
    },
    category: {
        '&:hover': {
            cursor: 'pointer',
            color: '#fe365f',
        },
    },
    secondaryCategory: {
        position: 'absolute',
        bottom: '5rem',
    },
    selectedCategory: {
        backgroundColor: '#3a3a42',
    },
    link: {
        color: theme.palette.text.primary,
        textUnderlineOffset: '0.3rem',
        fontSize: '1rem',
        textDecoration: 'none',
    },
}));
