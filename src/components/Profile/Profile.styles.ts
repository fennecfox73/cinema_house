import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.background.default,
        color: theme.palette.text.primary,
    },
    profileContent: {
        display: 'flex',
        height: '100vh',
        [theme.breakpoints.up('xs')]: {
            marginTop: '100px',
        },
        [theme.breakpoints.up('sm')]: {
            marginTop: '80px',
        },
    },
    categoryContentContainer: {
        display: 'flex',
        flexDirection: 'column',
        alignContent: 'center',
        width: '100%',
        alignItems: 'center',
        flexWrap: 'wrap',
        boxSizing: 'border-box',
    },
    title: {
        margin: '3rem auto',
        textAlign: 'center',
    },
}));
