import { Categories } from './Profile.types';

export const categories: Categories[] = ['Favourites', 'Explore', 'Watchlist', 'Settings', 'Log out'];
