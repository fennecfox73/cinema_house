import { AnyAction } from 'redux';

import {
    CATEGORY_SELECTED,
    USERNAME_SET,
    SHOULD_SHOW_WARNING_MESSAGE_SET,
    WARNING_MESSAGE_SET,
    GDPR_POPUP_BUTTON_CLICKED,
    FAVOURITE_MOVIE_ADDED,
    FAVOURITE_MOVIE_REMOVED,
    WARNING_POPUP_MESSAGE,
} from './Profile.actions';
import { Categories } from './Profile.types';
interface initialState {
    selectedCategory: Categories;
    username: string;
    setShouldShowWarningMessage: boolean;
    warningMessageSet: string;
    isGDPRPopUpButtonClicked: boolean;
    favouriteMovies: number[];
    warningPopupMessageSet: string;
}

const initialState: initialState = {
    selectedCategory: 'Favourites',
    username: '',
    setShouldShowWarningMessage: false,
    warningMessageSet: '',
    isGDPRPopUpButtonClicked: false,
    favouriteMovies: [],
    warningPopupMessageSet: '',
};

function profileReducers(state = initialState, action: AnyAction) {
    switch (action.type) {
        case CATEGORY_SELECTED:
            return {
                ...state,
                selectedCategory: action.selectedCategory,
            };
        case USERNAME_SET:
            return {
                ...state,
                username: action.username,
            };
        case SHOULD_SHOW_WARNING_MESSAGE_SET:
            return {
                ...state,
                setShouldShowWarningMessage: action.setShouldShowWarningMessage,
            };
        case WARNING_MESSAGE_SET:
            return {
                ...state,
                warningMessageSet: action.warningMessageSet,
            };
        case GDPR_POPUP_BUTTON_CLICKED:
            return {
                ...state,
                isGDPRPopUpButtonClicked: action.isGDPRPopUpButtonClicked,
            };
        case FAVOURITE_MOVIE_ADDED:
            return {
                ...state,
                favouriteMovies: state.favouriteMovies.concat(action.favMovie),
            };
        case FAVOURITE_MOVIE_REMOVED:
            return {
                ...state,
                favouriteMovies: state.favouriteMovies.filter(element => element !== action.favMovie),
            };
        case WARNING_POPUP_MESSAGE:
            return {
                ...state,
                warningPopupMessageSet: action.warningPopupMessageSet,
            };
        default:
            return state;
    }
}

export default profileReducers;
export type ProfileState = ReturnType<typeof profileReducers>;
