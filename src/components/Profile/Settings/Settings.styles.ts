import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        color: theme.palette.text.primary,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        gap: '1rem',
        textAlign: 'center',
        [theme.breakpoints.up('xs')]: {
            width: '95%',
        },
        [theme.breakpoints.up('sm')]: {
            width: '60%',
        },
        [theme.breakpoints.up('md')]: {
            width: '40%',
        },
    },
    subtitle: {
        margin: '1rem auto',
    },
    inputField: {
        backgroundColor: theme.palette.primary.light,
        borderRadius: '5px',
    },
    ensuingInputField: {
        marginTop: '50px',
    },
}));
