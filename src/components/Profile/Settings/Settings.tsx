import { IconButton, InputAdornment, TextField, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import useStyles from './Settings.styles';
import { getUserName } from '../../../utils/common.utils';
import WarningMessage from '../../GeneralComponents/WarningMessage/WarningMessage';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import CheckIcon from '@material-ui/icons/Check';
import SimpleButton from '../../GeneralComponents/SimpleButton/SimpleButton';
import cn from 'clsx';
import { Color } from '@material-ui/lab/Alert';
import { textConstants } from '../../../constants';

const Settings: React.FC = () => {
    const classes = useStyles();

    const [currentPassword, setCurrentPassword] = useState<string>('');
    const [newPassword, setNewPassword] = useState<string>('');
    const [showCurrentPassword, setShowCurrentPassword] = useState<boolean>(false);
    const [showNewPassword, setShowNewPassword] = useState<boolean>(false);
    const [showInfoMessage, setShowInfoMessage] = useState<boolean>(false);
    const [messageSeverity, setMessageSeverity] = useState<Color>('success');
    const [message, setMessage] = useState<string>('');

    const currentUser = getUserName();

    const showMessage = (warning: string, isSuccess = true) => {
        setMessage(warning);
        setShowInfoMessage(true);
        setMessageSeverity(isSuccess ? 'success' : 'error');
    };

    const changePasswordIfCorrectCredentials = async () => {
        const submittedUserData = {
            userName: currentUser,
            password: currentPassword,
            newPassword: newPassword,
        };

        fetch('http://localhost:8082/changepassword/', {
            method: 'POST',
            body: JSON.stringify(submittedUserData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }).then(async response => {
            const res = await response.json();

            if (response.status === 200) {
                showMessage(res.errorMsg);
            } else if (response.status === 404) {
                showMessage(res.errorMsg, false);
            } else {
                showMessage('Unexpected error occured. Try again.', false);
            }
        });
    };

    const handleResetPasswordButtonClick = (event: { preventDefault: () => void }) => {
        event.preventDefault();
        changePasswordIfCorrectCredentials();
    };

    const handleClickShowFirstPassword = () => {
        setShowCurrentPassword(!showCurrentPassword);
    };

    const handleClickShowThirdPassword = () => {
        setShowNewPassword(!showNewPassword);
    };

    const handleMouseDownPassword = (event: { preventDefault: () => void }) => {
        event.preventDefault();
    };

    return (
        <div className={classes.container}>
            <Typography className={classes.subtitle}>{textConstants.WANT_TO_CHANGE_PASSWORD}</Typography>
            {showInfoMessage && <WarningMessage severity={messageSeverity} message={message} />}
            <TextField
                label="Current password"
                type={showCurrentPassword ? 'text' : 'password'}
                onChange={e => setCurrentPassword(e.target.value)}
                variant="filled"
                className={classes.inputField}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton onClick={handleClickShowFirstPassword} onMouseDown={handleMouseDownPassword}>
                                {showCurrentPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
            />
            <TextField
                label="New password"
                type={showNewPassword ? 'text' : 'password'}
                onChange={e => setNewPassword(e.target.value)}
                variant="filled"
                className={cn([classes.inputField, classes.ensuingInputField])}
                InputProps={{
                    endAdornment: (
                        <InputAdornment position="end">
                            <IconButton onClick={handleClickShowThirdPassword} onMouseDown={handleMouseDownPassword}>
                                {showNewPassword ? <VisibilityOff /> : <Visibility />}
                            </IconButton>
                        </InputAdornment>
                    ),
                }}
            />
            <SimpleButton
                text={'Submit'}
                icon={<CheckIcon />}
                handleOnClick={handleResetPasswordButtonClick}
                isPrimary={true}
            />
        </div>
    );
};

export default Settings;
