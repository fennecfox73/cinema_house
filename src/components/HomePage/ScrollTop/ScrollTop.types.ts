import { ReactNode } from 'react';

export interface ScrollTopProps {
    children: ReactNode;
    // eslint-disable-next-line
    window: any;
}
