import React from 'react';
import { Fab } from '@material-ui/core';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import CatalogCards from '../CatalogCards/CatalogCards';
import ScrollTop from '../ScrollTop/ScrollTop';
import GDPRPopUp from '../../GDPR/GDPRPopUp/GDPRPopUp';
import WarningMessagePopup from '../../GeneralComponents/WarningMessagePopup/WarningMessagePopup';

// eslint-disable-next-line
const Catalog: React.FunctionComponent = (props: any) => {
    return (
        <>
            <div id="back-to-top-anchor" />
            <CatalogCards />
            <ScrollTop {...props}>
                <Fab color="secondary" size="small" aria-label="scroll back to top">
                    <KeyboardArrowUpIcon />
                </Fab>
            </ScrollTop>
            <GDPRPopUp />
            <WarningMessagePopup />
        </>
    );
};

export default Catalog;
