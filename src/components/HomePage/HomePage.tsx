import React from 'react';
import { AppBar } from '@material-ui/core';
import HeaderTopbar from '../GeneralComponents/Header/HeaderTopbar/HeaderTopbar';
import Catalog from './Catalog/Catalog';

const HomePage: React.FC = () => {
    return (
        <>
            <AppBar>
                <HeaderTopbar />
            </AppBar>
            <Catalog />
        </>
    );
};

export default HomePage;
