import React from 'react';
import { useRef, useEffect } from 'react';
import { Grid, CardMedia } from '@material-ui/core';
import '../../../App.scss';
import loadingSpinner from '../../../images/loading-spinner.gif';
import useIntersectionObserver from '../../../customHooks/useIntersectionObserver';
import { changePageSrolledTill } from '../../../main.actions';
import { fetchAllMovies, Movie } from '../../../services/movies.services';
import { showMoviesAtHomePage } from '../../../main.actions';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import Card from '../../GeneralComponents/Card/Card';
import useStyles from './CatalogCards.styles';
import { RootState } from '../../../store';
import { textConstants } from '../../../constants';

const CatalogCards: React.FC = () => {
    const loadingRef = useRef<HTMLDivElement | null>(null);
    const entry = useIntersectionObserver(loadingRef, {});
    const isVisible = !!entry?.isIntersecting;
    const dispatch = useDispatch();
    const movies = useSelector((state: RootState) => state.movies.homePageMovies);
    const searchedMovie = useSelector((state: RootState) => state.movies.searchedMovie);
    const pageSrolledTill = useSelector((state: RootState) => state.movies.pageSrolledTill);
    const classes = useStyles();

    useEffect(() => {
        if (isVisible) {
            if (pageSrolledTill <= 500) {
                dispatch(changePageSrolledTill(pageSrolledTill + 1));

                fetchAllMovies(String(pageSrolledTill))
                    .then(nextPage => {
                        dispatch(showMoviesAtHomePage([...movies, ...nextPage]));
                    })
                    .catch(() => {
                        dispatch(showMoviesAtHomePage([...movies]));
                    });
            }
        }
    }, [isVisible]);

    return (
        <div>
            {movies.length > 0 && movies.length < 6 && (
                <div className={classes.searchResultsTitle}>{textConstants.MOVIES_FOUND}</div>
            )}
            {movies.length > 0 ? (
                <Grid container className={classes.container}>
                    {movies
                        .filter((movie: Movie) => movie.vote_average !== 0)
                        .map((movie: Movie) => (
                            <Card key={movie.id} card={movie} />
                        ))}
                </Grid>
            ) : searchedMovie ? (
                <div className={classes.noResultsMessage}>{textConstants.TRY_DIFFERENT_PHRASE}</div>
            ) : (
                <CardMedia component="img" image={loadingSpinner} className={classes.loadingSpinner} />
            )}
            {!searchedMovie && (
                <div ref={loadingRef}>{pageSrolledTill <= 500 ? '' : textConstants.ALL_MOVIES_SEEN}</div>
            )}
        </div>
    );
};

export default CatalogCards;
