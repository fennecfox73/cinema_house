import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        justifyContent: 'center',
        marginTop: '100px',
        ['@media (min-width:830px)']: {
            marginTop: '60px',
        },
        [theme.breakpoints.up('md')]: {
            marginTop: '70px',
        },
    },
    noResultsMessage: {
        textAlign: 'center',
        color: theme.palette.text.primary,
    },
    loadingSpinner: {
        maxWidth: '70px',
        display: 'block',
        marginLeft: 'auto',
        marginRight: 'auto',
    },
    searchResultsTitle: {
        margin: '70px',
    },
}));
