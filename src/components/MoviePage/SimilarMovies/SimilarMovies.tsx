import React, { useState, useEffect } from 'react';
import { Movie, fetchSimilarMovies } from '../../../services/movies.services';
import CardsGrid from '../../GeneralComponents/CardsGrid/CardsGrid';
import { useSelector } from 'react-redux';
import { movieIdFromUrl } from '../../../utils/common.utils';
import useStyles from './SimilarMovies.styles';
import { RootState } from '../../../store';
import { textConstants } from '../../../constants';

const SimilarMovies: React.FC = () => {
    const classes = useStyles();

    const [similarMovies, setSimilarMovies] = useState<Movie[]>([]);

    const selectedMovie = useSelector((state: RootState) => state.movies.selectedMovie);

    useEffect(() => {
        const movieId = selectedMovie !== 0 ? selectedMovie : movieIdFromUrl();
        const callAPI = async () => {
            const fetchedSimilarMovies = await fetchSimilarMovies(movieId);
            setSimilarMovies(fetchedSimilarMovies);
        };

        callAPI();
    }, [selectedMovie]);

    return (
        <div className={classes.container}>
            {similarMovies.length > 0 && <div className={classes.title}>{textConstants.MORE_LIKE_THIS}</div>}
            <CardsGrid movies={similarMovies}></CardsGrid>
        </div>
    );
};

export default SimilarMovies;
