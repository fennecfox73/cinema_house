import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        '& div': {
            justifyContent: 'center',
        },
    },
    title: {
        margin: '2.5rem 0',
        textAlign: 'center',
    },
}));
