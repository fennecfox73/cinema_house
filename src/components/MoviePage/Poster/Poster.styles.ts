import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        [theme.breakpoints.up('xs')]: {
            maxWidth: '20%',
            margin: '2rem auto',
        },
        [theme.breakpoints.up('sm')]: {
            maxWidth: '80%',
            margin: '0 auto',
        },
    },
}));
