import React from 'react';
import { Card, CardMedia } from '@material-ui/core';
import noImage from '../../../images/no-image-available.png';
import useStyles from './Poster.styles';
import { PosterProps } from './Poster.types';

const Poster: React.FC<PosterProps> = ({ movie, movieImg }) => {
    const classes = useStyles();

    return (
        <Card className={classes.container}>
            <CardMedia
                component="img"
                alt={'Poster of ' + movie.title}
                image={movie.poster_path ? movieImg : noImage}
            />
        </Card>
    );
};

export default Poster;
