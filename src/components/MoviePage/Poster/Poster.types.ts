import { Movie } from '../../../services/movies.services';

export interface PosterProps {
    movie: Movie;
    movieImg: string;
}
