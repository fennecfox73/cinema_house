import React, { useEffect, useState } from 'react';
import { AppBar } from '@material-ui/core';
import { Grid } from '@material-ui/core';
import HeaderTopbar from '../GeneralComponents/Header/HeaderTopbar/HeaderTopbar';
import useStyles from './MoviePage.styles';
import Footer from '../GeneralComponents/Footer/Footer';
import GDPRPopUp from '../GDPR/GDPRPopUp/GDPRPopUp';
import SimilarMovies from './SimilarMovies/SimilarMovies';
import Poster from './Poster/Poster';
import Details from './Details/Details';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { initialMovieData } from '../../initialData/initialMovieData';
import { Movie, fetchSelectedMovie } from '../../services/movies.services';
import noImage from '../../images/no-image-available.png';
import { movieIdFromUrl } from '../../utils/common.utils';
import WarningMessagePopup from '../GeneralComponents/WarningMessagePopup/WarningMessagePopup';
const posterBaseUrl = 'https://image.tmdb.org/t/p/w300';

const MoviePage: React.FC = () => {
    const classes = useStyles();

    const selectedMovie = useSelector((state: RootState) => state.movies.selectedMovie);

    useEffect(() => {
        if ('scrollRestoration' in history) {
            history.scrollRestoration = 'auto';
        }
    });

    const [movie, setMovie] = useState<Movie>(initialMovieData);
    const [movieImg, setMovieImg] = useState<string>(noImage);
    const [movieId, setMovieId] = useState<number>(0);

    useEffect(() => {
        const movieId = selectedMovie !== 0 ? selectedMovie : movieIdFromUrl();
        setMovieId(selectedMovie !== 0 ? selectedMovie : movieIdFromUrl());
        const callAPI = async () => {
            const fetchedSelectedMovieInfo = await fetchSelectedMovie(movieId);
            setMovie(fetchedSelectedMovieInfo);

            if (fetchedSelectedMovieInfo.poster_path !== null) {
                setMovieImg(posterBaseUrl + fetchedSelectedMovieInfo.poster_path);
            }
        };

        callAPI();
        window.scrollTo(0, 0);
    }, [selectedMovie]);

    return (
        <>
            <AppBar>
                <HeaderTopbar></HeaderTopbar>
            </AppBar>
            <Grid container spacing={2} className={classes.container}>
                <Grid item xs={12} sm={4} md={3}>
                    <Poster movie={movie} movieImg={movieImg} />
                </Grid>
                <Grid item xs={12} sm={8} md={9}>
                    <Details movie={movie} movieId={movieId} />
                </Grid>
                <Grid item xs={12}>
                    <SimilarMovies />
                    <Footer />
                </Grid>
                <GDPRPopUp />
            </Grid>
            <WarningMessagePopup />
        </>
    );
};

export default MoviePage;
