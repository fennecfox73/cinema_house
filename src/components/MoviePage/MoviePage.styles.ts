import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        marginTop: '100px',
        color: theme.palette.text.primary,
    },
}));
