import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        fontSize: '2.3rem',
        fontWeight: 400,
        letterSpacing: '0.03em',
        marginTop: 0,
    },
    iconButton: {
        marginBottom: '0.5rem',
        marginLeft: '0.2rem',
    },
    icon: {
        fontSize: '2.5rem',
        color: theme.palette.text.primary,
    },
    errorMessage: {
        marginBottom: '2rem',
    },
}));
