import React from 'react';
import { IconButton, Typography } from '@material-ui/core';
import useStyles from './DetailsHeader.styles';
import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import FavoriteIcon from '@material-ui/icons/Favorite';
import { DetailsHeaderProps } from './DetailsHeader.types';
import { getUserName } from '../../../../utils/common.utils';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../../store';
import { favouriteMovie, removeFavouriteMovie } from '../../../../utils/movies.utils';

const DetailsHeader: React.FC<DetailsHeaderProps> = ({ movie, movieId }) => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const favouriteMovies = useSelector((state: RootState) => state.profile.favouriteMovies);

    const isMovieFaved = favouriteMovies.includes(movieId);
    const currentUser = getUserName();

    const changeIsMovieFavourite = () => {
        if (isMovieFaved) {
            dispatch(removeFavouriteMovie({ userName: currentUser, movie: movieId }));
        } else {
            dispatch(favouriteMovie({ userName: currentUser, movie: movieId }));
        }
    };

    return (
        <Typography className={classes.container} data-testid="movie-page-movie-title">
            {movie.title}
            <IconButton onClick={changeIsMovieFavourite} className={classes.iconButton}>
                {isMovieFaved ? (
                    <FavoriteIcon className={classes.icon} />
                ) : (
                    <FavoriteBorderIcon className={classes.icon} />
                )}
            </IconButton>
        </Typography>
    );
};

export default DetailsHeader;
