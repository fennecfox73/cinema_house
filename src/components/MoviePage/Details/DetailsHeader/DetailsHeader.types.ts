import { Movie } from '../../../../services/movies.services';

export interface DetailsHeaderProps {
    movie: Movie;
    movieId: number;
}
