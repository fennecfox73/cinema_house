import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        fontSize: '1rem',
        lineHeight: '1.75',
        letterSpacing: '0.01em',
    },
    imdb: {
        backgroundColor: theme.palette.background.default,
        padding: '2px',
        marginRight: '10px',
        '& a': {
            color: 'inherit',
            textDecoration: 'inherit',
        },
    },
    separator: {
        margin: '0 10px',
    },
    mainParagraph: {
        textAlign: 'justify',
    },
    mainParagraphTitle: {
        textAlign: 'justify',
    },
}));
