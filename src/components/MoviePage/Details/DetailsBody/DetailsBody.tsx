import React from 'react';
import { textConstants } from '../../../../constants';
import useStyles from './DetailsBody.styles';
import { DetailsBodyProps } from './DetailsBody.types';

const DetailsBody: React.FC<DetailsBodyProps> = ({ movie }) => {
    const classes = useStyles();

    const movieGenresAmount = movie.genres?.length ?? 0;

    const convertMinutesToHoursAndMinutes = (durationInMinutes: number) => {
        const remainingHours = Math.floor(durationInMinutes / 60);
        const remainingMinutes = durationInMinutes % 60;

        return remainingHours + remainingMinutes > 0 ? remainingHours + ' h ' + remainingMinutes + ' min' : ' 0 min';
    };

    return (
        <div className={classes.container}>
            <span className={classes.imdb}>
                <a href="https://www.imdb.com/title/tt2096673/" target="_blank" rel="noopener noreferrer">
                    {textConstants.IMDB}
                </a>
            </span>
            <span data-testid="movie-page-movie-vote-average">
                {movie.vote_average !== undefined && movie.vote_average.toFixed(2)}
            </span>
            <span data-testid="movie-page-movie-release-date">
                {movie.release_date ? (
                    <>
                        <span className={classes.separator}>|</span>
                        {movie.release_date.substring(0, 4)} (
                        {movie.production_countries && movie.production_countries[0]?.iso_3166_1})
                    </>
                ) : (
                    'Release date: Coming soon'
                )}
            </span>
            <span>
                {movie.runtime && (
                    <>
                        <span className={classes.separator}>|</span>
                        <span data-testid="movie-page-movie-duration">
                            {convertMinutesToHoursAndMinutes(movie.runtime)}
                        </span>
                    </>
                )}
            </span>
            <p className={classes.mainParagraph} data-testid="movie-page-movie-overview">
                {movie.overview}
            </p>
            <p>
                <span className={classes.mainParagraphTitle}>{textConstants.GENRES}:</span>
                &nbsp;
                {movie.genres &&
                    movie.genres?.map(
                        (
                            genre: {
                                name:
                                    | boolean
                                    | React.ReactChild
                                    | React.ReactFragment
                                    | React.ReactPortal
                                    | null
                                    | undefined;
                            },
                            i: React.Key | null | undefined,
                        ) => (
                            <span key={i}>
                                {genre.name}
                                {movieGenresAmount !== Number(i) + 1 && <span>,</span>}&nbsp;
                            </span>
                        ),
                    )}
            </p>
            {Boolean(movie.budget) && (
                <p>
                    <span className={classes.mainParagraphTitle}>{textConstants.BUDGET}:</span> ${movie.budget}
                </p>
            )}
            {Boolean(movie.revenue) && (
                <p>
                    <span className={classes.mainParagraphTitle}>{textConstants.REVENUE}:</span> ${movie.revenue}
                </p>
            )}
        </div>
    );
};

export default DetailsBody;
