import { Movie } from '../../../../services/movies.services';

export interface DetailsBodyProps {
    movie: Movie;
}
