import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        margin: 'auto 2rem',
        [theme.breakpoints.up('sm')]: {
            margin: '0',
            paddingRight: '5rem',
        },
        [theme.breakpoints.up('md')]: {
            paddingRight: '2rem',
        },
    },
}));
