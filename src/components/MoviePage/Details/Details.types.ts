import { Movie } from '../../../services/movies.services';

export interface DetailsProps {
    movie: Movie;
    movieId: number;
}
