import React from 'react';
import DetailsHeader from './DetailsHeader/DetailsHeader';
import { DetailsProps } from './Details.types';
import DetailsBody from './DetailsBody/DetailsBody';
import useStyles from './Details.styles';

const Details: React.FC<DetailsProps> = ({ movie, movieId }) => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <DetailsHeader movie={movie} movieId={movieId} />
            <DetailsBody movie={movie} />
        </div>
    );
};

export default Details;
