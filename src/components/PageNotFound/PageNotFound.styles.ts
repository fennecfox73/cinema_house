import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textAlign: 'center',
        color: theme.palette.text.primary,
    },
    descriptionContainer: {
        marginBottom: '4rem',
    },
    title: {
        fontSize: '6rem',
        [theme.breakpoints.down('sm')]: {
            fontSize: '3rem',
        },
    },
    subtitle: {
        fontSize: '2.5rem',
        [theme.breakpoints.down('sm')]: {
            fontSize: '1.5rem',
        },
    },
    button: {
        marginBottom: '4rem',
        '& a': {
            textDecoration: 'none',
        },
    },
}));
