import Typography from '@material-ui/core/Typography';
import React from 'react';
import ButtonWithLink from '../GeneralComponents/ButtonWithLink/ButtonWithLink';
import useStyles from './PageNotFound.styles';
import HomeIcon from '@material-ui/icons/Home';

const PageNotFound: React.FC = () => {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <div className={classes.descriptionContainer}>
                <Typography className={classes.title}>Oops!</Typography>
                <Typography className={classes.subtitle}>404 - page not found</Typography>
            </div>
            <div className={classes.button}>
                <ButtonWithLink linkForNavigation={'/'} text={'Go to home page'} icon={<HomeIcon />} />
            </div>
        </div>
    );
};

export default PageNotFound;
