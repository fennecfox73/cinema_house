import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.text.primary,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        width: '40%',
        [theme.breakpoints.down('md')]: {
            width: '60%',
        },
        [theme.breakpoints.down('sm')]: {
            width: '80%',
        },
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
        padding: '4rem',
        gap: '1rem',
        borderRadius: '6px',
        textAlign: 'center',
    },
    title: {
        margin: '0 auto 25px',
    },
    button: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.text.primary,
        textTransform: 'capitalize',
        fontSize: '1rem',
        '&:hover': {
            backgroundColor: '#f59cae',
        },
    },
    buttonIcon: {
        fontSize: '1.2rem',
        marginLeft: '10px',
    },
    closeIcon: {
        position: 'absolute',
        right: '0',
        top: '0',
        margin: '15px',
        color: theme.palette.text.primary,
    },
    signUpMessage: {
        marginTop: '50px',
        fontSize: '1rem',
    },
    inputField: {
        backgroundColor: theme.palette.primary.light,
        color: theme.palette.text.primary,
        borderRadius: '5px',
    },
    link: {
        color: theme.palette.text.primary,
        textUnderlineOffset: '0.3rem',
        fontSize: '1rem',
        margin: '2.5rem auto 0',
        '&:hover': {
            cursor: 'pointer',
            color: '#fe365f',
        },
    },
}));
