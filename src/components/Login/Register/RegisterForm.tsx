import { Box, IconButton, InputAdornment, Modal, TextField, Typography } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import useStyles from './RegisterForm.styles';
import { useDispatch, useSelector } from 'react-redux';
import InputIcon from '@material-ui/icons/Input';
import Close from '@material-ui/icons/Close';
import { setShouldShowWarningMessage, setUsername, setWarningMessage } from '../../Profile/Profile.actions';
import { NavLink, useNavigate } from 'react-router-dom';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import WarningMessage from '../../GeneralComponents/WarningMessage/WarningMessage';
import { RootState } from '../../../store';
import SimpleButton from '../../GeneralComponents/SimpleButton/SimpleButton';
import { textConstants } from '../../../constants';

const RegisterForm: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const showWarningMessage = useSelector((state: RootState) => state.profile.setShouldShowWarningMessage);
    const warningMessage = useSelector((state: RootState) => state.profile.warningMessageSet);

    const [email, setEmail] = useState<string>('');
    const [password, setPassword] = useState<string>('');
    const [userName, setUserName] = useState<string>('');
    const [showPassword, setShowPassword] = useState<boolean>(false);

    // call handleSignupButtonClick() when Enter key is pressed
    useEffect(() => {
        const keyDownHandler = (event: { key: string; preventDefault: () => void }) => {
            if (event.key === 'Enter') {
                event.preventDefault();
                handleSignupButtonClick(event);
            }
        };

        document.addEventListener('keydown', keyDownHandler);

        return () => {
            document.removeEventListener('keydown', keyDownHandler);
        };
    }, []);

    const setUserPassword = (password: React.SetStateAction<string>) => {
        setPassword(password);
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event: { preventDefault: () => void }) => {
        event.preventDefault();
    };

    const showWarning = (warning: string) => {
        dispatch(setWarningMessage(warning));
        dispatch(setShouldShowWarningMessage(true));
    };

    const isEmailValid = () => {
        const validMailFormat = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
        if (email.match(validMailFormat)) {
            return true;
        } else {
            showWarning('Email address is incorrect');
            return false;
        }
    };

    const isDataValid = () => {
        if (!isEmailValid()) {
            showWarning('Email address is incorrect');
            return false;
        } else if (password.length < 6 || password.length > 20) {
            showWarning('Password should be between 6 and 20 characters');
            return false;
        } else if (userName.length < 6 || userName.length > 20) {
            showWarning('User name should be between 6 and 20 characters');
            return false;
        }
        return true;
    };

    const postNewUserIfEmailDoesntExist = async () => {
        const newUserData = {
            userName: userName,
            email: email,
            password: password,
        };

        fetch('http://localhost:8082/register/', {
            method: 'POST',
            body: JSON.stringify(newUserData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }).then(async response => {
            if (response.status === 403) {
                const res = await response.json();
                showWarning(res.errorMsg);
            } else {
                const user = {
                    username: userName,
                    isLoggedIn: true,
                };
                localStorage.setItem('user', JSON.stringify(user));

                dispatch(setUsername(userName));
                navigate('/profile');
                return response.text();
            }
        });
    };

    const handleSignupButtonClick = (event: { preventDefault: () => void }) => {
        event.preventDefault();
        isDataValid() && postNewUserIfEmailDoesntExist();
    };

    const discardWarningMessages = () => {
        dispatch(setWarningMessage(''));
        dispatch(setShouldShowWarningMessage(false));
    };

    return (
        <Modal open={true}>
            <Box className={classes.container}>
                <NavLink to={'/'} className={classes.closeIcon}>
                    <Close />
                </NavLink>
                <Typography variant="h4" className={classes.title}>
                    Become <br />a Cinema Houser!
                </Typography>
                <>
                    {showWarningMessage && <WarningMessage message={warningMessage} />}
                    <TextField
                        label="E-mail"
                        onChange={e => setEmail(e.target.value)}
                        variant="filled"
                        className={classes.inputField}
                    />
                    <TextField
                        label="Password"
                        type={showPassword ? 'text' : 'password'}
                        onChange={e => setUserPassword(e.target.value)}
                        variant="filled"
                        className={classes.inputField}
                        InputProps={{
                            endAdornment: (
                                <InputAdornment position="end">
                                    <IconButton onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}>
                                        {showPassword ? <VisibilityOff /> : <Visibility />}
                                    </IconButton>
                                </InputAdornment>
                            ),
                        }}
                    />
                    <TextField
                        label="User name"
                        onChange={e => setUserName(e.target.value)}
                        variant="filled"
                        className={classes.inputField}
                    />
                    <SimpleButton
                        text={'Sign up'}
                        icon={<InputIcon />}
                        handleOnClick={handleSignupButtonClick}
                        isPrimary={true}
                    />
                </>
                <NavLink to={'/login'} className={classes.link} onClick={discardWarningMessages}>
                    {textConstants.ALREADY_USER}
                </NavLink>
            </Box>
        </Modal>
    );
};

export default RegisterForm;
