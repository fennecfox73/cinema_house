import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    container: {
        backgroundColor: theme.palette.primary.dark,
        color: theme.palette.text.primary,
        display: 'flex',
        flexWrap: 'wrap',
        flexDirection: 'column',
        width: '40%',
        position: 'absolute',
        left: '50%',
        top: '50%',
        transform: 'translate(-50%, -50%)',
        padding: '7rem 4rem 4rem',
        gap: '1rem',
        borderRadius: '6px',
        textAlign: 'center',
        [theme.breakpoints.down('sm')]: {
            width: '70%',
        },
        [theme.breakpoints.down('xs')]: {
            width: '95%',
        },
    },
    button: {
        backgroundColor: theme.palette.primary.main,
        color: theme.palette.text.primary,
        textTransform: 'capitalize',
        fontSize: '1rem',
        '&:hover': {
            backgroundColor: '#f59cae',
        },
    },
    buttonIcon: {
        fontSize: '1.2rem',
        marginLeft: '10px',
    },
    closeIcon: {
        position: 'absolute',
        right: '0',
        top: '0',
        margin: '15px',
        color: theme.palette.text.primary,
    },
    link: {
        color: theme.palette.text.primary,
        fontSize: '1rem',
        textUnderlineOffset: '0.3rem',
        '&:hover': {
            cursor: 'pointer',
            color: '#fe365f',
        },
    },
    linkSecondary: {
        color: theme.palette.text.primary,
        fontSize: '0.8rem',
        marginBottom: '20px',
        '&:hover': {
            cursor: 'pointer',
            color: '#fe365f',
        },
    },
    inputField: {
        backgroundColor: theme.palette.primary.light,
        borderRadius: '5px',
    },
    ensuingInputField: {
        marginTop: '50px',
    },
}));
