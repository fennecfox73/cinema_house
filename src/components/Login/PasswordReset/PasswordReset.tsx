import { Box, IconButton, InputAdornment, Modal, TextField } from '@material-ui/core';
import React, { useState } from 'react';
import useStyles from './PasswordReset.styles';
import Close from '@material-ui/icons/Close';
import { NavLink } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { setShouldShowWarningMessage, setWarningMessage } from '../../Profile/Profile.actions';
import WarningMessage from '../../GeneralComponents/WarningMessage/WarningMessage';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import CheckIcon from '@material-ui/icons/Check';
import SimpleButton from '../../GeneralComponents/SimpleButton/SimpleButton';
import cn from 'clsx';
import { textConstants } from '../../../constants';

const PasswordReset: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();

    const [password, setPassword] = useState<string>('');
    const [showPassword, setShowPassword] = useState<boolean>(false);

    const showWarningMessage = useSelector((state: RootState) => state.profile.setShouldShowWarningMessage);
    const warningMessage = useSelector((state: RootState) => state.profile.warningMessageSet);

    const handleResetPasswordButtonClick = (event: { preventDefault: () => void }) => {
        event.preventDefault();
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event: { preventDefault: () => void }) => {
        event.preventDefault();
    };

    const discardWarningMessages = () => {
        dispatch(setWarningMessage(''));
        dispatch(setShouldShowWarningMessage(false));
    };

    return (
        <Modal open={true}>
            <Box className={classes.container}>
                <NavLink to={'/'} className={classes.closeIcon}>
                    <Close />
                </NavLink>
                {showWarningMessage && <WarningMessage message={warningMessage} />}
                <TextField
                    label="Current password"
                    type={showPassword ? 'text' : 'password'}
                    onChange={e => setPassword(e.target.value)}
                    variant="filled"
                    className={classes.inputField}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}>
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <TextField
                    label="Repeat current password"
                    type={showPassword ? 'text' : 'password'}
                    onChange={e => setPassword(e.target.value)}
                    variant="filled"
                    className={classes.inputField}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}>
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <TextField
                    label="New password"
                    type={showPassword ? 'text' : 'password'}
                    onChange={e => setPassword(e.target.value)}
                    variant="filled"
                    className={cn([classes.inputField, classes.ensuingInputField])}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}>
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <SimpleButton
                    text={'Submit'}
                    icon={<CheckIcon />}
                    handleOnClick={handleResetPasswordButtonClick}
                    isPrimary={true}
                />
                <NavLink to={'/login'} className={classes.link} onClick={discardWarningMessages}>
                    <p>{textConstants.LOG_IN}</p>
                </NavLink>
                <NavLink to={'/register'} className={classes.link} onClick={discardWarningMessages}>
                    <p>{textConstants.CREATE_ACCOUNT}</p>
                </NavLink>
            </Box>
        </Modal>
    );
};

export default PasswordReset;
