import { Box, IconButton, InputAdornment, Modal, TextField, Typography } from '@material-ui/core';
import React, { useState } from 'react';
import useStyles from './LoginForm.styles';
import InputIcon from '@material-ui/icons/Input';
import Close from '@material-ui/icons/Close';
import { NavLink, useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '../../../store';
import { setShouldShowWarningMessage, setUsername, setWarningMessage } from '../../Profile/Profile.actions';
import WarningMessage from '../../GeneralComponents/WarningMessage/WarningMessage';
import { Visibility, VisibilityOff } from '@material-ui/icons';
import SimpleButton from '../../GeneralComponents/SimpleButton/SimpleButton';
import { textConstants } from '../../../constants';
import cn from 'clsx';

const LoginForm: React.FC = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const navigate = useNavigate();

    const [password, setPassword] = useState<string>('');
    const [user, setUser] = useState<string>('');
    const [showPassword, setShowPassword] = useState<boolean>(false);

    const showWarningMessage = useSelector((state: RootState) => state.profile.setShouldShowWarningMessage);
    const warningMessage = useSelector((state: RootState) => state.profile.warningMessageSet);

    const showWarning = (warning: string) => {
        dispatch(setWarningMessage(warning));
        dispatch(setShouldShowWarningMessage(true));
    };

    const loginIfUserExists = async (event: { preventDefault: () => void }) => {
        event.preventDefault();

        const submittedUserData = {
            username: user,
            password: password,
        };

        fetch('http://localhost:8082/login/', {
            method: 'POST',
            body: JSON.stringify(submittedUserData),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        }).then(async response => {
            const res = await response.json();

            if (response.status === 200) {
                const user = {
                    username: res.username,
                    isLoggedIn: true,
                };
                localStorage.setItem('user', JSON.stringify(user));

                dispatch(setUsername(res.username));
                dispatch(setShouldShowWarningMessage(false));
                navigate('/');
            } else if (response.status === 404) {
                showWarning(res.errorMsg);
            } else {
                showWarning('Unexpected error occured. Try again.');
            }
        });
    };

    const handleClickShowPassword = () => {
        setShowPassword(!showPassword);
    };

    const handleMouseDownPassword = (event: { preventDefault: () => void }) => {
        event.preventDefault();
    };

    const discardWarningMessages = () => {
        dispatch(setWarningMessage(''));
        dispatch(setShouldShowWarningMessage(false));
    };

    return (
        <Modal open={true}>
            <Box className={classes.container}>
                <NavLink to={'/'} className={classes.closeIcon}>
                    <Close />
                </NavLink>
                <Typography variant="h4" className={classes.title}>
                    {textConstants.LOG_IN}
                </Typography>
                {showWarningMessage && <WarningMessage message={warningMessage} />}
                <TextField
                    onChange={e => setUser(e.target.value)}
                    label="Username"
                    variant="filled"
                    className={classes.inputField}
                />
                <TextField
                    label="Password"
                    type={showPassword ? 'text' : 'password'}
                    onChange={e => setPassword(e.target.value)}
                    variant="filled"
                    className={classes.inputField}
                    InputProps={{
                        endAdornment: (
                            <InputAdornment position="end">
                                <IconButton onClick={handleClickShowPassword} onMouseDown={handleMouseDownPassword}>
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        ),
                    }}
                />
                <NavLink to={''} className={cn(classes.link, classes.secondaryLink)} onClick={discardWarningMessages}>
                    <p>{textConstants.FORGOT_PASSWORD}</p>
                </NavLink>
                <SimpleButton text={'Log in'} icon={<InputIcon />} handleOnClick={loginIfUserExists} isPrimary={true} />
                <NavLink to={'/register'} className={classes.link} onClick={discardWarningMessages}>
                    {textConstants.CREATE_ACCOUNT}
                </NavLink>
            </Box>
        </Modal>
    );
};

export default LoginForm;
