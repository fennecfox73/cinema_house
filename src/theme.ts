import React from 'react';

export type PaletteMode = 'light' | 'dark';

export const getTheme = (mode: PaletteMode) => ({
    palette: {
        mode,
        secondary: {
            main: '#C68A77',
        },
        error: {
            light: '#e5a0a0',
            main: '#CD5C5C',
        },
        common: {
            white: '#fff',
        },
        ...(mode === 'dark'
            ? {
                  // for dark mode
                  background: {
                      default: '#1d1d27',
                      light: '#626276',
                  },
                  text: {
                      primary: '#fff',
                      secondary: '#fff',
                  },
                  primary: {
                      main: '#fe365f',
                      contrastText: '#fff',
                      light: '#626276',
                      dark: '#2b2b36',
                  },
              }
            : {
                  // for light mode
                  background: {
                      default: '#e4e5f1',
                      light: '#626276',
                  },
                  text: {
                      // TO CHANGE
                      primary: '#000',
                      secondary: '#fff',
                  },
                  primary: {
                      main: '#fe365f',
                      // TO CHANGE
                      contrastText: '#000',
                      // TO CHANGE
                      light: '#626276',
                      dark: '#2b2b36',
                  },
              }),
    },
    typography: {
        fontFamily: ['Roboto', 'Helvetica', 'Arial', 'sans-serif'].join(','),
        fontWeight: {
            body: 400,
            heading: 900,
            bold: 700,
        },
    },
    overrides: {
        MuiSwitch: {
            switchBase: {
                color: '#636161',
            },
            colorSecondary: {
                '&$checked': {
                    color: '#fe365f',
                },
            },
            track: {
                opacity: 0.9,
                backgroundColor: '#c9c9c9',
                '$checked$checked + &': {
                    opacity: 1,
                    backgroundColor: '#494444',
                },
            },
        },
    },
});
