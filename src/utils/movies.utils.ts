import { addFavMovie, removeFavMovie, setWarningPopupMessage } from '../components/Profile/Profile.actions';

export const removeFavouriteMovie = (request: { userName?: string; movie: number }) => {
    return async (dispatch: (arg0: { type: string; favMovie?: number; warningPopupMessageSet?: string }) => void) => {
        const response = await fetch('http://localhost:8082/removefavouritemovie/', {
            method: 'POST',
            body: JSON.stringify(request),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        });
        if (response.status === 200) {
            dispatch(removeFavMovie(request.movie));
        } else {
            dispatch(setWarningPopupMessage('An error occured removing movie from favourites.'));
        }
    };
};

export const favouriteMovie = (request: { userName?: string; movie: number }) => {
    return async (dispatch: (arg0: { type: string; favMovie?: number; warningPopupMessageSet?: string }) => void) => {
        const response = await fetch('http://localhost:8082/favouritemovie/', {
            method: 'POST',
            body: JSON.stringify(request),
            headers: {
                'Content-type': 'application/json; charset=UTF-8',
            },
        });
        if (response.status === 200) {
            dispatch(addFavMovie(request.movie));
        } else {
            dispatch(setWarningPopupMessage('An error occured removing movie from favourites.'));
        }
    };
};
