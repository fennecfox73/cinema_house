export const movieIdFromUrl = (): number => {
    return Number(document.URL.split('/').pop());
};

export const isUserLoggedIn = (): boolean => {
    const currentUser = localStorage.getItem('user');
    const currentUserStringify = currentUser ? JSON.parse(currentUser) : {};
    const isUserLoggedIn = currentUserStringify.isLoggedIn !== undefined ? currentUserStringify.isLoggedIn : false;
    return isUserLoggedIn;
};

export const getUserName = (): string => {
    const currentUser = localStorage.getItem('user');
    const currentUserStringify = currentUser ? JSON.parse(currentUser) : {};
    const username = currentUserStringify.isLoggedIn !== undefined ? currentUserStringify.username : '';
    return username;
};
