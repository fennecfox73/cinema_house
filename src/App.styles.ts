import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
    root: {
        color: theme.palette.text.primary,
        fontSize: '1.5rem',
    },
    container: {
        display: 'flex',
        justifyContent: 'center',
        flexDirection: 'column',
        paddingTop: '5px',
        paddingBottom: '5px',
        minHeight: '100vh',
        minWidth: '100vw',
    },
    typographyBase: {
        fontFamily: theme.typography.fontFamily,
    },
}));
