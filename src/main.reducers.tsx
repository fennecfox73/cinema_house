import { AnyAction } from 'redux';

import {
    HOME_PAGE_MOVIES_ADDED,
    MOVIE_SELECTED,
    MOVIE_PAGE_OPENED,
    SEARCHED_MOVIE_SET,
    CURRENT_PAGE_SET,
    IS_DARK_MODE_ON,
} from './main.actions';
import { initialState } from './main.state';

function mainReducer(state = initialState, action: AnyAction) {
    switch (action.type) {
        case HOME_PAGE_MOVIES_ADDED:
            return {
                ...state,
                homePageMovies: action.homePageMovies,
            };
        case MOVIE_SELECTED:
            return {
                ...state,
                selectedMovie: action.selectedMovie,
            };
        case MOVIE_PAGE_OPENED:
            return {
                ...state,
                isMoviePageOpened: action.isMoviePageOpened,
            };
        case SEARCHED_MOVIE_SET:
            return {
                ...state,
                searchedMovie: action.searchedMovie,
            };
        case CURRENT_PAGE_SET:
            return {
                ...state,
                pageSrolledTill: action.pageSrolledTill,
            };
        case IS_DARK_MODE_ON:
            return {
                ...state,
                isDarkMode: action.isDarkMode,
            };
        default:
            return state;
    }
}

export default mainReducer;
export type MainState = ReturnType<typeof mainReducer>;
