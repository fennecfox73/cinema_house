import { combineReducers } from 'redux';
import mainReducer from './main.reducers';
import profileReducers from './components/Profile/Profile.reducers';

const rootReducer = combineReducers({
    movies: mainReducer,
    profile: profileReducers,
});

export default rootReducer;
