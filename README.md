<h1>🍿🍿🍿 CINEMA HOUSE 🏡  🏡  🏡</h1>

<h3>Check out Cinema House:</h3>
<a href="https://cinemahouse.herokuapp.com" target="_blank">cinemahouse.herokuapp.com</a>
<br />
<br />

<h3>My motivation to create this project</h3>
This project was created to learn using MERN stack and to deploy a full-stack project using Heroku. 🤘🏻👏 <br />
<br />

<h3>😎 Building this project I learnt to:</h3>
- Use React.js <br />
- Use Redux <br />
- Create BE with Node.js, Express.js <br />
- Use MongoDB Atlas and MongoDB Compass <br />
- Copy data from a public API to a JSON file (by writting a short Python script) <br />
- Use a public API <br />
- Deploy to Heroku <br />
- Migrate projects from GitHub to GitLab <br />
- Create a CI pipeline in GitLab <br />
- Write unit test using Jest/Enzyme <br />
- Use Material UI styles (CSS-in-JS) <br />
- Create Mobile first responsive design <br />
- Optimise React performance <br />
- Use cookies and localStorage <br />
- Encrypt passwords <br />
- Create register and login flows <br />
- Save data for different users <br />
- SEO for React.js <br />
- Have fun along the way! 🕺💃 <br />
<br />
