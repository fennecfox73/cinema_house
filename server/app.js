import express from 'express';
import connectDB from './config/db.js';
import movie from './routes/movie.js';
import user from './routes/user/user.js';
import favouriteMovies from './routes/user/favouriteMovies/favouriteMovies.js';

const app = express();

app.use(express.json());

// Enable CORS
app.use(function (req, res, next) {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    next();
});

app.use('/', movie);
app.use('/', user);
app.use('/', favouriteMovies);

// Connect Database
connectDB();

// eslint-disable-next-line no-undef
const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));
