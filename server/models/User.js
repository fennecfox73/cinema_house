import mongoose, { Schema } from 'mongoose';

export const UserSchema = new Schema({
    userName: {
        type: String,
        required: false,
        unique: true,
    },
    email: {
        type: String,
        required: true,
        unique: true,
    },
    password: {
        type: String,
        required: true,
    },
    runtime: {
        type: Number,
        required: false,
    },
    date: {
        type: Date,
        default: Date.now,
    },
    favouriteMovies: {
        type: [Number],
        required: false,
    },
});

export default mongoose.model('User', UserSchema);
