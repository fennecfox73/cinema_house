import mongoose, { Schema } from 'mongoose';

export const MovieSchema = new Schema({
    id: {
        type: Number,
        required: true,
    },
    title: {
        type: String,
        required: true,
    },
    vote_average: {
        type: Number,
        required: false,
    },
    overview: {
        type: String,
        required: false,
    },
    poster_path: {
        type: String,
        required: false,
    },
    release_date: {
        type: String,
        required: false,
    },
    runtime: {
        type: Number,
        required: false,
    },
    budget: {
        type: Number,
        required: false,
    },
    revenue: {
        type: Number,
        required: false,
    },
    genres: {
        type: {
            id: Number,
            name: String,
        },
        required: false,
    },
    production_countries: {
        type: {
            iso_3166_1: String,
            name: String,
        },
        required: false,
    },
});

export default mongoose.model('Movie', MovieSchema);
