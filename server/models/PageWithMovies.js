import mongoose, { Schema } from 'mongoose';
import { MovieSchema } from '../models/Movie.js';

const PageWithMoviesSchema = new Schema({
    page: {
        type: Number,
        required: true,
    },
    results: [MovieSchema],
    total_pages: {
        type: Number,
        required: false,
    },
    total_results: {
        type: Number,
        required: false,
    },
});

export default mongoose.model('PageWithMovies', PageWithMoviesSchema);
