import express from 'express';
import User from '../../models/User.js';
import { hashPassword, verifyPassword } from '../utils.js';

const router = express.Router();

router.post('/register', async (req, res) => {
    const user = await User.findOne({ email: req.body.email });
    const password = await hashPassword(req.body.password);

    try {
        if (user) {
            return res.status(403).json({ errorMsg: 'A user with this email already exists' });
        } else {
            const newUser = new User({
                userName: req.body.userName,
                email: req.body.email,
                password: password,
            });
            newUser.save();
            return res.status(200).json({ msg: newUser });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/login', async (req, res) => {
    try {
        const user = await User.findOne({ email: req.body.username });
        const verified = await verifyPassword(req.body.password, user.password);

        if (user && verified) {
            return res.status(200).json({ username: user.userName });
        } else {
            return res.status(404).json({ errorMsg: 'A user with these credentials does not exist' });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/changepassword', async (req, res) => {
    try {
        const user = await User.findOne({ userName: req.body.userName });
        const verified = await verifyPassword(req.body.password, user.password);

        if (verified) {
            const newPassword = await hashPassword(req.body.newPassword);
            user.password = newPassword;
            user.save();
            return res.status(200).json({ errorMsg: 'Password was changed successfully.' });
        } else {
            return res.status(404).json({ errorMsg: 'Current password is incorrect.' });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

export default router;
