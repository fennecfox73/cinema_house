import express from 'express';
import User from '../../../models/User.js';

const router = express.Router();

router.post('/favouritemovie', async (req, res) => {
    try {
        const user = await User.findOne({ userName: req.body.userName });

        if (req.body.movie) {
            const alreadyFavedMovie = await User.findOne({ favouriteMovies: req.body.movie });
            if (!alreadyFavedMovie) {
                await User.updateOne({ userName: user.userName }, { $push: { favouriteMovies: req.body.movie } });
                user.save();

                return res.status(200).json({ errorMsg: 'Movie added to favourites.' });
            } else {
                return res.status(409).json({ errorMsg: 'Movie is already in favourites list.' });
            }
        } else {
            return res.status(404).json({ errorMsg: 'Something went wrong.' });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/removefavouritemovie', async (req, res) => {
    try {
        const user = await User.findOne({ userName: req.body.userName });

        if (req.body.movie) {
            const favouriteMovie = await User.findOne({ favouriteMovies: req.body.movie });
            if (favouriteMovie) {
                await User.updateOne({ userName: user.userName }, { $pull: { favouriteMovies: req.body.movie } });
                user.save();
                return res.status(200).json({ errorMsg: 'Movie removed from favourites.' });
            } else {
                return res.status(404).json({ errorMsg: 'Movie is not in favourites list.' });
            }
        } else {
            return res.status(404).json({ errorMsg: 'Something went wrong.' });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

router.post('/favouritedmovies', async (req, res) => {
    try {
        const user = await User.findOne({ userName: req.body.userName });

        if (user) {
            return res.status(200).json(user.favouriteMovies);
        } else {
            return res.status(404).json({ errorMsg: 'Something went wrong.' });
        }
    } catch (error) {
        res.status(500).send(error);
    }
});

export default router;
