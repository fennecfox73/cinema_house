import express from 'express';
import Movie from '../models/Movie.js';
import PageWithMovies from '../models/PageWithMovies.js';

const router = express.Router();

router.post('/pagesWithMovies', async (req, res) => {
    try {
        PageWithMovies.create(req.body).then(movie => res.json({ msg: `Movie ${movie} added` }));
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

router.get('/pagesWithMovies/:page', async (req, res) => {
    try {
        const search_key = req.params.page;
        const posts = await PageWithMovies.find({ page: search_key });
        res.json(posts[0]);
    } catch (error) {
        res.status(400).json({ message: error.message });
    }
});

export default router;
