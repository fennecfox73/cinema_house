describe('Dark / Light mode', () => {
    before(() => {
        // cy.visit('http://localhost:3000');
        cy.visit('/');
    });

    const checkIfDarkModeOn = (isDarkMode = true) => {
        cy.get('[data-testid=header]')
            .should('have.attr', 'data-testid', 'header')
            .and('have.css', 'background-color', isDarkMode ? 'rgb(29, 29, 39)' : 'rgb(228, 229, 241)');
    };

    it('should switch between dark and light modes', () => {
        checkIfDarkModeOn();

        cy.get('[data-testid=header-mode-switch]').click({ force: true });

        checkIfDarkModeOn(false);

        cy.get('[data-testid=catalog-card]').first().click({ force: true });

        checkIfDarkModeOn(false);

        cy.get('[data-testid=header-mode-switch]').click({ force: true });

        checkIfDarkModeOn();
    });
});
