describe('Dark / Light mode', () => {
    before(() => {
        cy.visit('/');
    });

    it('should switch between dark and light modes', () => {
        cy.get('[data-testid=header-search-box]').type('tom{enter}');

        cy.get('[data-testid=catalog-card]').should('have.length.above', 0);
    });
});
