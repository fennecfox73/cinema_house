describe('Movie page', () => {
    before(() => {
        cy.visit('/');
    });

    it('should open movie page and display movie info correcrtly', () => {
        cy.get('[data-testid=catalog-card]').invoke('attr', 'href').as('firstMovie');

        cy.get('[data-testid=catalog-card]').first().click({ force: true });

        cy.get('[data-testid=movie-page-movie-title]').should('not.be.empty');
        cy.get('[data-testid=movie-page-movie-vote-average]').should('not.be.empty');
        cy.get('[data-testid=movie-page-movie-release-date]').should('not.be.empty');
        cy.get('[data-testid=movie-page-movie-duration]').should('not.be.empty');
        cy.get('[data-testid=movie-page-movie-overview]').should('not.be.empty');
        cy.get('[data-testid=catalog-card]').should('have.length', 20);

        cy.get('[data-testid=catalog-card]').invoke('attr', 'href').as('secondMovie');

        // check if navigates to another movie
        // after clicking the first element from similar movies list
        expect('@firstMovie').to.not.equal('@secondMovie');
    });
});
